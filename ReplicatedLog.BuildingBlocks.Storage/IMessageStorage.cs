﻿using ReplicatedLog.Domain;
using System;

namespace ReplicatedLog.BuildingBlocks.Storage
{
    public interface IMessageStorage: IStorage<Message>
    {
    }
}
