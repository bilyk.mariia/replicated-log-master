﻿using System.Collections;
using System.Collections.Generic;

namespace ReplicatedLog.BuildingBlocks.Storage
{
    public interface IStorage<T> where T: class
    {
        void Append(T item);

        List<T> GetAll();

        T GetLast();
    }
}