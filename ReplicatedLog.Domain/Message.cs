﻿using System;

namespace ReplicatedLog.Domain
{
    public class Message
    {
        public long Id { get; set; }
        public string MessageText { get; set; }
        public DateTime ReceivedAt { get; set; }

    }
}
