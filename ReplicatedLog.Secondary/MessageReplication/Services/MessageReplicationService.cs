﻿using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

using ReplicatedLogGrcp;

namespace MessageReplication.Services
{
    public class MessageReplicationService : ReplicatedLogGrcp.MessageReplication.MessageReplicationBase
    {
        private readonly ILogger<MessageReplicationService> _logger;

        public MessageReplicationService(ILogger<MessageReplicationService> logger)
        {
            _logger = logger;
        }

        public override Task<SecondaryResponse> ReplicateMessage(MessageAppendRequest request, ServerCallContext context)
        {
            var message = $"Got a message with ID {request.Id}, thanks!";
            _logger.LogInformation(message);

            return Task.FromResult(new SecondaryResponse { Success = true, ResponseText = message });
        }
    }
}
