#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-nanoserver-1903 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-nanoserver-1903 AS build
WORKDIR /src
COPY ["ReplicatedLog.Master.Api/ReplicatedLog.Master.Api.csproj", "ReplicatedLog.Master.Api/"]
COPY ["ReplicatedLog.BuildingBlocks.Storage.InMemory/ReplicatedLog.BuildingBlocks.Storage.InMemory.csproj", "ReplicatedLog.BuildingBlocks.Storage.InMemory/"]
COPY ["ReplicatedLog.BuildingBlocks.Storage/ReplicatedLog.BuildingBlocks.Storage.csproj", "ReplicatedLog.BuildingBlocks.Storage/"]
COPY ["ReplicatedLog.Domain/ReplicatedLog.Domain.csproj", "ReplicatedLog.Domain/"]
RUN dotnet restore "ReplicatedLog.Master.Api/ReplicatedLog.Master.Api.csproj"
COPY . .
WORKDIR "/src/ReplicatedLog.Master.Api"
RUN dotnet build "ReplicatedLog.Master.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ReplicatedLog.Master.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ReplicatedLog.Master.Api.dll"]