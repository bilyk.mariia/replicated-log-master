﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api.ApiModels;
using ReplicatedLog.Master.Api.Services.Interfaces;

namespace ReplicatedLog.Master.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly ILogger<MessagesController> _logger;
        private readonly IMessageStorage _messageStorage;
        private readonly IDataReplicationWriteService _dataReplicationWriteService;

        public MessagesController(ILogger<MessagesController> logger, IMessageStorage messageStorage, IDataReplicationWriteService dataReplicationWriteService)
        {
            _logger = logger;
            _messageStorage = messageStorage;
            _dataReplicationWriteService = dataReplicationWriteService;
        }

        [HttpPost]
        public async Task<IActionResult> AppendMessage(MessageApiModel model)
        {
            var lastItemAppended = _messageStorage.GetLast();
            var fullModel = new Message
            {
                Id = lastItemAppended?.Id + 1 ?? 1,
                MessageText = model.MessageText,
                ReceivedAt = DateTime.UtcNow
            };
            
            _messageStorage.Append(fullModel);
            _logger.LogInformation($"Message with id {fullModel.Id} appended to master db.");

            //TODO: implement passing W param
            var saveModelToSecondariesSuccess = await _dataReplicationWriteService.SendMessageToSecondaries(fullModel);
            _logger.LogInformation($"Message with id {fullModel.Id} replicated to secondaries with success - {saveModelToSecondariesSuccess}.");

            return !saveModelToSecondariesSuccess ? StatusCode(500) : Ok();
        }

        [HttpGet]
        public IActionResult GetMessages()
        {
            var messages = _messageStorage.GetAll();
            return Ok(messages);
        }
    }
}
