﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ReplicatedLog.Domain;
using ReplicatedLog.Master.Api.Services.Interfaces;
using ReplicatedLog.Master.Api.Settings;
using ReplicatedLogGrcp;

namespace ReplicatedLog.Master.Api.Services
{
    public class DataReplicationWriteService : IDataReplicationWriteService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<DataReplicationWriteService> _logger;
        private readonly MessageReplication.MessageReplicationClient _client;

        public DataReplicationWriteService(IConfiguration configuration,
            ILogger<DataReplicationWriteService> logger,
            MessageReplication.MessageReplicationClient client)
        {
            _configuration = configuration;
            _logger = logger;
            _client = client;
        }

        public async Task<bool> SendMessageToSecondaries(Message message)
        {
            var secondariesSettings = _configuration
                .GetSection(SettingsConstants.ReplicationSecondariesSettingsName)
                .Get<List<ReplicationSecondarySettings>>();

            var messageRequestModel = new MessageAppendRequest
            {
                Id = message.Id,
                MessageText = message.MessageText,
                ReceivedAt = Timestamp.FromDateTime(message.ReceivedAt)
            };

            var secondariesRequests =
                secondariesSettings.Select(settings => SendMessage(settings, messageRequestModel));
               
            var responses = await Task.WhenAll(secondariesRequests);

            return responses.All(r => r);
        }

        private async Task<bool> SendMessage(ReplicationSecondarySettings replicationSecondary,
            MessageAppendRequest messageRequestModel)
        {
            ////AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2Support", true);
            //var httpClient = new HttpClient();
            //using var channel = GrpcChannel.ForAddress(replicationSecondary.WriteEndpoint);
            //var client = new MessageReplication.MessageReplicationClient(channel);
            SecondaryResponse response;
            try
            {
                response = await _client.ReplicateMessageAsync(messageRequestModel);
            }
            catch (Exception e)
            {
                return LogErrorAndReturn(replicationSecondary, e.Message);
            }
            
            return response.Success || LogErrorAndReturn(replicationSecondary, response.ResponseText);
        }

        private bool LogErrorAndReturn(ReplicationSecondarySettings replicationSecondary, string errorMessage)
        {
            _logger.LogError(
                $"Call to secondary with name:{replicationSecondary.Name} failed with error {errorMessage}.");
            return false;
        }
    }
}