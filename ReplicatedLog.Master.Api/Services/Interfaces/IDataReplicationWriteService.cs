﻿using System.Threading.Tasks;
using ReplicatedLog.Domain;

namespace ReplicatedLog.Master.Api.Services.Interfaces
{
    public interface IDataReplicationWriteService
    {
        // TODO: add W parameter here and this service will know everything about W param
        Task<bool> SendMessageToSecondaries(Message message);
    }
} 