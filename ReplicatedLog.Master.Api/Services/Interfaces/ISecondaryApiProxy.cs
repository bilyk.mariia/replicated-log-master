﻿using System.Threading.Tasks;
using ReplicatedLog.Domain;

namespace ReplicatedLog.Master.Api.Services.Interfaces
{
    public interface ISecondaryApiProxy
    {
        // TODO: should this service know about url?
        Task SendMessage(Message message);
    }
}