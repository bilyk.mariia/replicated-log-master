﻿using System.ComponentModel.DataAnnotations;

namespace ReplicatedLog.Master.Api.ApiModels
{
    public class MessageApiModel
    {
        [Required]
        public string MessageText { get; set; }
    }
}
