using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ReplicatedLog.BuildingBlocks.Storage;
using ReplicatedLog.BuildingBlocks.Storage.InMemory;
using ReplicatedLog.Master.Api.Services;
using ReplicatedLog.Master.Api.Services.Interfaces;
using ReplicatedLog.Master.Api.Settings;
using ReplicatedLogGrcp;

namespace ReplicatedLog.Master.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var secondariesSettings = Configuration.GetSection(SettingsConstants.ReplicationSecondariesSettingsName)
                .Get<List<ReplicationSecondarySettings>>();

            var secondaryEndpoint = secondariesSettings.FirstOrDefault()?.WriteEndpoint;

            services.AddGrpcClient<MessageReplication.MessageReplicationClient>(o =>
            {
                o.Address = new Uri(secondaryEndpoint);
            });

            services.AddSingleton<IMessageStorage, MessageStorage>();
            services.AddTransient<IDataReplicationWriteService, DataReplicationWriteService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
